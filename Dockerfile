FROM python:3.7-alpine3.8

COPY * /weather-app/

RUN pip3 install --user -r /weather-app/requirements.txt 

ENTRYPOINT python3 /weather-app/weather.py
